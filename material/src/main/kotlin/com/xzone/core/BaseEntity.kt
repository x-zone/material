package com.xzone.core

import com.baomidou.mybatisplus.annotation.FieldFill
import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableLogic
import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

abstract class BaseEntity(
    @TableId
    val id: String = UUID.randomUUID().toString().replace("-", ""),

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    val createTime: Date = Date(),

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    var updateTime: Date? = null,

    @TableField(value = "is_deleted")
    @TableLogic(value = "false", delval = "true")
    val deleted: Boolean = false
)
