package com.xzone.core

import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(Exception::class)
    fun exceptionHandler(e: Exception): Any {
        // TODO 异常记录，发送通知
        e.printStackTrace()

        val result = HashMap<String, Any>()
        result["code"] = 500
        result["msg"] = "系统异常"
        return result
    }
}