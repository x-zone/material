package com.xzone.material.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User

class MyUser(val userId: String, username: String, password: String, authorities: Collection<GrantedAuthority>) :
    User(username, password, authorities)