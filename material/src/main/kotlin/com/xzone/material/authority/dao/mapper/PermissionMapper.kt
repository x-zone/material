package com.xzone.material.authority.dao.mapper

import com.baomidou.mybatisplus.core.mapper.BaseMapper
import com.xzone.material.authority.domain.entity.PermissionEntity

interface PermissionMapper : BaseMapper<PermissionEntity>