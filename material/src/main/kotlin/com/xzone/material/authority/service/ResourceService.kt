package com.xzone.material.authority.service

import com.xzone.material.authority.dao.ResourceDao
import com.xzone.material.authority.domain.entity.ResourceEntity
import org.springframework.stereotype.Service

@Service
class ResourceService(val resourceDao: ResourceDao) {
    fun list(): List<ResourceEntity> {
        return this.resourceDao.list()
    }
}