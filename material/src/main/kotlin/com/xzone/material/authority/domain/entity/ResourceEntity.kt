package com.xzone.material.authority.domain.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.xzone.core.BaseEntity

@TableName("`sec_resource`")
data class ResourceEntity(val code: String, var name: String) : BaseEntity()
