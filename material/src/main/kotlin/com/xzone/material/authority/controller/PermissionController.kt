package com.xzone.material.authority.controller

import com.xzone.material.authority.domain.entity.PermissionEntity
import com.xzone.material.authority.service.PermissionService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("permission")
class PermissionController(val permissionService: PermissionService) {
    @GetMapping("list")
    fun list(): List<PermissionEntity> {
        return this.permissionService.list()
    }
}