package com.xzone.material.authority.dao

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl
import com.xzone.material.authority.dao.mapper.RoleMapper
import com.xzone.material.authority.domain.entity.RoleEntity
import org.springframework.stereotype.Repository

@Repository
class RoleDao : ServiceImpl<RoleMapper, RoleEntity>()