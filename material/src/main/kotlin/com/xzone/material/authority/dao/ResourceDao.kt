package com.xzone.material.authority.dao

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl
import com.xzone.material.authority.dao.mapper.ResourceMapper
import com.xzone.material.authority.domain.entity.ResourceEntity
import org.springframework.stereotype.Repository

@Repository
class ResourceDao : ServiceImpl<ResourceMapper, ResourceEntity>()