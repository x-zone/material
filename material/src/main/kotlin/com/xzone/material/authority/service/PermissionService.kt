package com.xzone.material.authority.service

import com.xzone.material.authority.dao.PermissionDao
import com.xzone.material.authority.domain.entity.PermissionEntity
import org.springframework.stereotype.Service

@Service
class PermissionService(val permissionDao: PermissionDao) {
    fun getUserPermissionCodeList(userId: String): Set<String> {
        // TODO 1.查询用户角色 2.查询角色资源 3.查询资源权限
//        return setOf("test", "abc")
        return emptySet()
    }

    fun list(): List<PermissionEntity> {
        return this.permissionDao.list()
    }
}