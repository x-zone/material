package com.xzone.material.authority.dao

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl
import com.xzone.material.authority.dao.mapper.PermissionMapper
import com.xzone.material.authority.domain.entity.PermissionEntity
import org.springframework.stereotype.Repository

@Repository
class PermissionDao : ServiceImpl<PermissionMapper, PermissionEntity>()