package com.xzone.material.authority.dao.mapper

import com.baomidou.mybatisplus.core.mapper.BaseMapper
import com.xzone.material.authority.domain.entity.ResourceEntity

interface ResourceMapper : BaseMapper<ResourceEntity>