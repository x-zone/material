package com.xzone.material.authority.controller

import com.xzone.material.authority.domain.entity.RoleEntity
import com.xzone.material.authority.service.RoleService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("role")
class RoleController(val roleService: RoleService) {
    @GetMapping("list")
    fun list(): List<RoleEntity> {
        return this.roleService.list()
    }
}