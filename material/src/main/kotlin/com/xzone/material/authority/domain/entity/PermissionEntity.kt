package com.xzone.material.authority.domain.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.xzone.core.BaseEntity

@TableName("`sec_permission`")
data class PermissionEntity(val code: String) : BaseEntity()
