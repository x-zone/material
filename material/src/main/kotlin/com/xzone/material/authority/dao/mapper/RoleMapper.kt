package com.xzone.material.authority.dao.mapper

import com.baomidou.mybatisplus.core.mapper.BaseMapper
import com.xzone.material.authority.domain.entity.RoleEntity

interface RoleMapper : BaseMapper<RoleEntity>