package com.xzone.material.authority.service

import com.xzone.material.authority.dao.RoleDao
import com.xzone.material.authority.domain.entity.RoleEntity
import org.springframework.stereotype.Service

@Service
class RoleService(val roleDao: RoleDao) {
    fun list(): List<RoleEntity> {
        return this.roleDao.list()
    }
}