package com.xzone.material

import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.xzone")
@MapperScan("com.xzone.**.dao.mapper")
class MaterialApplication

fun main(args: Array<String>) {
    runApplication<MaterialApplication>(*args)
}
