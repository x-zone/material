package com.xzone.material.user.domain.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.xzone.core.BaseEntity

@TableName("`sec_user`")
data class UserEntity(
    var nickname: String = "",
    var account: String = "",
    var mobile: String? = null,
    var email: String? = null,
    var password: String = "",
) : BaseEntity()
