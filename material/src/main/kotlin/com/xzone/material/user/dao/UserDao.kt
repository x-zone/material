package com.xzone.material.user.dao

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl
import com.xzone.material.user.dao.mapper.UserMapper
import com.xzone.material.user.domain.entity.UserEntity
import org.springframework.stereotype.Repository

@Repository
class UserDao : ServiceImpl<UserMapper, UserEntity>()