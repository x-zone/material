package com.xzone.material.user.controller

import com.xzone.material.user.domain.param.UserSaveParam
import com.xzone.material.user.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("user")
class UserController(val userService: UserService) {

    @GetMapping("test")
    fun test(): String {
        return "test"
    }

    @GetMapping("hello")
    fun hello(): String {
        return "hello"
    }

    @GetMapping("info")
    fun info(): String {
        return "info"
    }

    @GetMapping("manage")
    fun manage(): String {
        return "manage"
    }

    /**
     * 保存数据
     *
     * @param saveParam 保存所需要的参数
     * @return 保存的结果，包括保存时生成的内容，如主键id
     */
    @PostMapping("save")
    fun save(@RequestBody saveParam: UserSaveParam): HashMap<String, Any> {

        val userEntity = this.userService.save(saveParam)

        val result = HashMap<String, Any>()

        result["data"] = userEntity

        return result
    }
}
