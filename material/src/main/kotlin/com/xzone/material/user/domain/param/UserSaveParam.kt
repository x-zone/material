package com.xzone.material.user.domain.param

data class UserSaveParam(
    val nickname: String,
    val account: String,
    val mobile: String? = null,
    val email: String? = null,
    val password: String,
)