package com.xzone.material.user.dao.mapper

import com.baomidou.mybatisplus.core.mapper.BaseMapper
import com.xzone.material.user.domain.entity.UserEntity

interface UserMapper : BaseMapper<UserEntity>